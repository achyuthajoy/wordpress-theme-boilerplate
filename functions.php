<?php 
/**
 * Enqueue Styles & Scripts
 */
function theme_styles_scripts() {
    wp_enqueue_script('jquery');
    wp_enqueue_style( 'main-css', get_stylesheet_directory_uri().'/css/main.css' );
    
    wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'theme_styles_scripts' );

/**
 * Remove Admin Bar from Front-end
 */
add_filter('show_admin_bar', '__return_false');